# Signal Logger

## Overview

These packages provide an interface for a signal logger and some default signal loggers. A signal logger can log a time series of a signal.

The software has been tested under ROS Kinetic and Ubuntu 16.04.

The source code is released under a [BSD 3-Clause license](LICENSE).

**Author(s):** Gabriel Hottiger, Christian Gehring, Dario Bellicoso

[Documentation](http://docs.leggedrobotics.com/signal_logger_doc/)

## Building

[![Build Status](https://ci.leggedrobotics.com/buildStatus/icon?job=bitbucket_leggedrobotics/signal_logger/master)](https://ci.leggedrobotics.com/job/bitbucket_leggedrobotics/job/signal_logger/job/master/)

In order to install, clone the latest version from this repository into your catkin workspace and compile the packages.

## Bugs & Feature Requests

Please report bugs and request features using the [Issue Tracker](https://bitbucket.org/leggedrobotics/signal_logger/issues).